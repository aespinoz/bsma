# Biología de Sistemas de Microorganismos Ambientales  - Practical on shotgun metagenomics, 9 November 2018

This repository has been specifically developed for

The repository contains all the third-part software and dependencies to run the practical on a small metagenomic dataset. 


The following programs are used for the analysis:


Quality Filtering: Sickle

Assembly: IDBA-UD

Gene prediction: Prodigal

Annotation: DIAMOND

Alignment: Bowtie2

Alignment file manipulation: Samtools, Bedtools

Taxonomic assignemts and parsing: MEGAN, in-house scripts

Graphical output: Krona 



Documentation for installing and running the analyses is avilable at: http://bsma.readthedocs.io/en/latest/


