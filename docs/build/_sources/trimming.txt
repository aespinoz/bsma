Quality trimminig
=================

Use Sickle to filter out low quality reads. First create the directory for storing quality-filtered reads::

	mkdir -p $wd/$PRJ_NAME/1_FilteredReads


Have a look to Sickle help::

	sickle pe --help

Now run Sickle on your data::

	cd $INPUT_DIR
	for i in mock1 mock2 mock3; do
		sickle pe \
       		-f $i.R1.fastq \
       		-r $i.R2.fastq \
       		-t sanger \
      		-o $FILT_READS/filtered.$i.R1.fastq \
       		-p $FILT_READS/filtered.$i.R2.fastq \
       		-s $FILT_READS/filtered.single.$i.fastq \
       		-q $AV_READS_Q \
        	-l $AV_READS_L
	done
