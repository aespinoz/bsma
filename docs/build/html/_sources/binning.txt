Binning
==============

**Bin the contigs in bins to obtain partial reconstructed genomes**

Source the configuration file::

	source conf_cluster.conf

Write the abundance list file::

	for i in mock1 mock2 mock3; do cat $FILT_READS/filtered.$i.R1.fastq $FILT_READS/filtered.$i.R2.fastq $FILT_READS/filtered.single.$i.fastq > $FILT_READS/cat.filtered.$i.fastq; echo $FILT_READS/cat.filtered.$i.fastq >> list_file_maxbin.txt; done


Create output folder for binning output::
	
	mkdir -p $BINN
	mv list_file_maxbin.txt  $BINN
	cd $BINN

Run MaxBin on the co-assembly::

	run_MaxBin.pl -contig $ASSEMB_READS/d.$PRJ_NAME.idba/contig.fa -reads_list list_file_maxbin.txt -out bin.$PRJ_NAME

