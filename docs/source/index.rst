Biología de Sistemas de Microorganismos Ambientales  - Practical on shotgun metagenomics, 9 November 2018
==========================================

Welcome to the practical at **Biología de Sistemas de Microorganismos Ambientales  - Practical on shotgun metagenomics, Valparaiso 9 November 2018**

This practical aims at running a complete bioinformatic analysis of a metagenomics dataset.

The practical is based of the software repository https://gitlab.com/andrea.franzetti/bsma, which contains all the third-part software and dependencies to run a complete bioinformatic analysis of a metagenomics dataset. 
The following programs are used for the analysis:

* Quality Filtering: Sickle (https://github.com/najoshi/sickle)
* Assembly: IDBA-UD (https://github.com/loneknightpy/idba)
* Gene prediction: Prodigal (https://github.com/hyattpd/Prodigal)
* Annotation: DIAMOND (https://github.com/bbuchfink/diamond)
* Alignment: Bowtie2 (http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
* Alignment file manipulation: Samtools, Bedtools (http://samtools.sourceforge.net/; http://bedtools.readthedocs.io/en/latest/)
* Taxonomic assignemts and parsing: MEGAN, Krona, in-house scritps (http://ab.inf.uni-tuebingen.de/software/megan6/; https://github.com/marbl/Krona/wiki)
* Graphical output: Krona


The present documentation helps the students to run the whole pipiline on the computer cluster.

Contents:

.. toctree::
   :maxdepth: 2

   software   
   trimming
   assembly
   binning
   prodigal
   annotation
   mapping 
   parsing
   mining	
